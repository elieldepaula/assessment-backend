<?php

namespace App\Helpers;

/**
 * Classe helper para abstrair a utilizaçao das sessoes. Assim fica facil de adotar
 * outro mecanismo de persistencia, como o Redis.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class SessionHelper
{
    
    /**
     * Salva uma variavel ou arrays de variaveis na sessao.
     * 
     * @param string|array $key String ou array de valores para a sessao.
     * @param mixed $value Valor que devera ser salvo na sessao.
     */
    public static function set($key, $value = null)
    {
        $session = [];
        if (is_array($key))
        {
            foreach ($key as $k => $v)
                $session[$k] = $v;
        } else
            $session[$key] = $value;
        $_SESSION['mySession'] = $session;
    }
    
    /**
     * Retorna um valor de uma posiçao na sessao.
     * 
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        $tempSession = isset($_SESSION['mySession']) ? $_SESSION['mySession'] : [];
        return isset($tempSession[$key]) ? $tempSession[$key] : null;
    }
    
    /**
     * Retorna uma posiçao da sessao e ja exclui sua referencia. Ideal para os
     * casos de mensagens temporarias.
     * 
     * @param string $key
     * @return mixed
     */
    public static function getTemp($key)
    {
        $value = SessionHelper::get($key);
        SessionHelper::del($key);
        return $value;
    }
    
    /**
     * Apaga uma posiçao da sessao.
     * @param string $key
     */
    public static function del($key)
    {
        unset($_SESSION['mySession'][$key]);
    }
    
    /**
     * Destroi a sessao toda.
     */
    public static function destroy()
    {
        $_SESSION['mySession'] = null;
        unset($_SESSION['mySession']);
    }
    
}
