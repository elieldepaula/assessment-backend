<?php

namespace App\Controllers;
use App\Repositories\ProductRepository as Product;
use App\Repositories\CategoryRepository as Category;
use App\Helpers\InputHelper as Input;
use App\Helpers\SessionHelper;

/**
 * Controller com as funcionalidades para gerenciar os produtos.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class ProductsController extends BaseController 
{
    
    public function all() 
    {
        
        $product        = new Product();
        $productList    = $product->list();
        
        $this->setVar('pageTitle', 'Products');
        $this->setVar('msg', SessionHelper::getTemp('msg'));
        $this->setVar('productList', $productList);
        $this->render('products.html');
        
    }
    
    public function create() 
    {
        
        $category       = new Category();
        $categoryList   = $category->list(); 
        
        $this->setVar('formAction', 'products/create');
        $this->setVar('pageTitle', 'New Product');
        $this->setVar('categoryList', $categoryList);
        $this->render('formProduct.html');
        
    }
    
    public function edit($id) 
    {
        
        $category       = new Category();
        $categoryList   = $category->list();
        
        $product = new Product();
        $prdEdit  = $product->find($id);

        $tempArr = [];
        foreach($prdEdit->category as $cat){
            array_push($tempArr, $cat->id);
        }
        
        foreach($categoryList as $cat){
            (in_array($cat->id, $tempArr) ? $cat->selected = true : $cat->selected = false);
        }
        
        $this->setVar('formAction', 'products/'.$id.'/edit');
        $this->setVar('pageTitle', 'Edit Product');
        $this->setVar('categoryList', $categoryList);
        $this->setVar('product', $prdEdit);
        $this->render('formProduct.html');
        
    }
    
    public function save($id = null) 
    {
        
        $product = new Product();
        $object = (object) [
            'sku' => Input::Post('sku'),
            'name' => Input::Post('name'),
            'description' => Input::Post('description'),
            'categories' => isset($_POST['categories']) ? $_POST['categories'] : null,
            'price' => Input::Post('price'),
            'qty' => Input::Post('qty'),
            'image' => $_FILES['image']
        ];

        if ($id===null) {
            
            if ($product->new($object)){
                $this->log('Product created successfully!', ['username' => 'Undefined', 'productName' => $object->name]);
                SessionHelper::set('msg', 'Product created successfully!');
                header('Location: /products');
            } else {
                $this->log('Product created successfully!', ['username' => 'Undefined', 'productName' => $object->name]);
                SessionHelper::set('msg', 'Error creating product.');
                header('Location: /products');
            }
            
        } else {
            
            if ($product->edit($object, $id)){
                $this->log('Product updated successfully!', ['username' => 'Undefined', 'productName' => $object->name]);
                SessionHelper::set('msg', 'Product updated successfully!');
                header('Location: /products');
            } else {
                $this->log('Error updating product', ['username' => 'Undefined', 'productName' => $object->name]);
                SessionHelper::set('msg', 'Error updating product.');
                header('Location: /products');
            }
            
        }
        
    }
    
    public function delete($id) 
    {
        $product = new Product();
        if ($product->del($id)){
            $this->log('Porduct deleted successfully!', ['username' => 'Undefined', 'id' => $id]);
            SessionHelper::set('msg', 'Product deleted successfully!');
            header('Location: /products');
        } else {
            $this->log('Error deleting product', ['username' => 'Undefined', 'id' => $id]);
            SessionHelper::set('msg', 'Error deleting product.');
            header('Location: /products');
        }
    }
    
}
