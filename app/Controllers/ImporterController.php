<?php

namespace App\Controllers;
use App\Repositories\CategoryRepository as Category;
use App\Repositories\ProductRepository as Product;

/**
 * Este controler e usado somente pelo front-controller do importador de produtos.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class ImporterController extends BaseCli
{
    
    private $fileName;
    private $delimiter = ';';
    private $enclosure = ' ';
    
    /**
     * Processa e importa o arquivo CSV indicado no argumento --file
     */
    public function import()
    {
        
        $this->fileName  = $this->options['file'];
        $this->log('Importando arquivo CSV', ['username' => 'undefined', 'filename' => $this->fileName]);
        
        //TODO: Melhorar o encapsulamento das opçoes dos argumentos para ficar mais reutilizavel.
        // $this->delimiter = (isset($this->options['delimiter']) and $this->options['delimiter'] != '' ? : ';');
        // $this->enclosure = (isset($this->options['enclosure']) and $this->options['enclosure'] != '' ? : ' ');
        
        echo "\n=========================================================================\n";
        echo "| Importador de produtos                                                |\n";
        echo "=========================================================================\n\n";
        echo "Importando arquivo " . $this->fileName . "\n";
        echo "------------------------------------------------------------------------\n\n";

        $fileStream = fopen($this->fileName, 'r');
        if ($fileStream) {
            $header = fgetcsv($fileStream, 0, $this->delimiter, $this->enclosure);
            $total     = 0;
            $totalOk   = 0;
            $totalErro = 0;
            while (!feof($fileStream)) {
                $line = fgetcsv($fileStream, 0, $this->delimiter, $this->enclosure);
                if (!$line)
                    continue;
                $row = array_combine($header, $line);
                $category = isset($row['categoria']) ? $this->getCategories($row['categoria']) : null;
                $product = new Product();
                $object = (object) [
                    'sku' => $row['sku'],
                    'name' => $row['nome'],
                    'description' => $row['descricao'],
                    'categories' => $category,
                    'price' => number_format($row['preco'], 2, ',', '.'),
                    'qty' => $row['quantidade']
                ];
                $total++;
                if ($product->new($object)){
                    $totalOk++;
                    echo "[OK]" . $row['nome'] . PHP_EOL;
                } else {
                    $totalErro++;
                    echo "[Erro]" . $row['nome'] . PHP_EOL;
                }
            }
            fclose($fileStream);
        }
        
        echo "\n\n------------------------------------------------------------------------\n\n";
        echo "Sucessos: $totalOk\n";
        echo "Erros:    $totalErro\n";
        echo "Total:    $total\n\n";
        echo "Concluido!\n\n";
        
        $this->log('Finalizaçao da importaçao do arquivo CSV', ['username' => 'undefined', 'filename' => $this->fileName, 'success' => $totalOk, 'errors' => $totalErro, 'total' => $total]);
        
    }
    
    /**
     * Este metodo processa as categorias do arquivo CSV que estao separadas
     * pelo caractere "pipe" | e transforma em um array que o sistema consegue
     * reconhecer e criar o relacionamento N:M entre categorias e produtos.
     * 
     * @param string $var
     * @return array
     */
    private function getCategories($var)
    {
        $arrCategory = explode('|', $var);
        $arrOutput = [];
        for ($i=0; $i <= count($arrCategory); $i++) {
            $object = new Category();
            $cat = @$object->whereOne($arrCategory[$i]);
            if (isset($cat->id)) {
                $cat->id;
                $arrOutput[] = $cat->id;
            } else {
                if (@$arrCategory[$i] != '') {
                     $object = (object) [
                        'name' => $arrCategory[$i],
                        'code' => ''
                    ];
                    $newObject = new Category();
                    $tempObj = $newObject->newImport($object);
                    $arrOutput[] = $tempObj->id;
                    unset($newObject);
                }
            }
            unset($cat);
            unset($object);
        }
        return $arrOutput;
    }
    
}
