<?php

namespace App\Repositories;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;
use App\Interfaces\CrudInterface;

/**
 * Repositorio de produtos, abstrai a operaçao do model para os controllers.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class ProductRepository implements CrudInterface
{
    
    protected $colection;
    
    function __construct()
    {
        $this->colection = new Product();
    }
    
    /**
     * Lista todos os registros.
     */
    public function list() : Collection
    {
        return $this->colection->with('category')->get();
    }
    
    /**
     * Retorna um registro.
     * @param int $id
     */
    public function find(int $id)
    {
        return $this->colection->find($id);
    }
    
    /*
     * Cria um novo registro.
     */
    public function new(object $object)
    {
        $this->colection->sku = $object->sku;
        $this->colection->name = $object->name;
        $this->colection->description = $object->description;
        $this->colection->price = $object->price;
        $this->colection->qty = $object->qty;
        if(isset($object->image['name']))$this->colection->image = $this->upload($object->image);
        $result = $this->colection->save();
        if(isset($object->categories))$this->colection->category()->attach($object->categories);
        return $result;
    }
    
    /**
     * Edita um registro.
     * 
     * @param Int $id ID do registro que sera editado.
     */
    public function edit(object $object, Int $id = null)
    {
        $product = $this->colection->find($id);
        $product->sku = $object->sku;
        $product->name = $object->name;
        $product->description = $object->description;
        $product->price = $object->price;
        $product->qty = $object->qty;
        $product->image = $object->image ? $this->upload($object->image) : null;
        $product->category()->detach();
        $product->category()->attach($object->categories);
        
        return $product->save();
    }
    
    /**
     * Exclui um registro.
     * @param Int $id ID do registro que sera editado.
     */
    public function del(Int $id = null)
    {
        return $this->colection->destroy($id);
    }

    /**
     * Faz o upload das imagens dos produtos.
     * 
     * @param array $file
     * @return boolean|string
     */
    private function upload($file)
    {
        $dir = BASE_PATH . "public/images/"; 
        $fileName = time() .'_' . str_replace(' ', '_', $file["name"]);
        if (move_uploaded_file($file["tmp_name"], "$dir/".$fileName)) { 
            return $fileName;
        } else
            return false;
    }

}
