<?php

/**
 * Este arquivo faz o carregamento do projeto, ele sobe o autoload do Composer,
 * faz a instancia do roteador como aplicaçao e mantem as rotas do projeto.
 * 
 * Uma boa pratica seria separar as rotas em um arquivo separado caso o projeto
 * tome maiores proporçoes.
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */

session_start();

require BASE_PATH . 'vendor/autoload.php';

require BASE_PATH . 'app/Database.php';

/**
 * Instancia o roteador.
 */
$app = System\App::instance();
$app->request = System\Request::instance();
$app->route = System\Route::instance($app->request);
$route = $app->route;

/**
 * Cria as rotas da aplicaçao.
 */

/**
 * Pagina inicial.
 */
$route->get(['/', 'index', 'home', 'default'], 'App\Controllers\MainController@dashboard');

/**
 * Grupo de rotas dos produtos.
 */
$route->group('/products', function(){
    $this->get ('/',            'App\Controllers\ProductsController@all');
    $this->get ('/create',      'App\Controllers\ProductsController@create');
    $this->post('/create',      'App\Controllers\ProductsController@save');
    $this->get ('/{id}/edit',   'App\Controllers\ProductsController@edit');
    $this->post('/{id}/edit',   'App\Controllers\ProductsController@save');
    $this->get ('/{id}/delete', 'App\Controllers\ProductsController@delete');
});

/**
 * Grupo de rotas das categorias de produtos.
 */
$route->group('/categories', function(){
    $this->get ('/',            'App\Controllers\CategoriesController@all');
    $this->get ('/create',      'App\Controllers\CategoriesController@create');
    $this->post('/create',      'App\Controllers\CategoriesController@save');
    $this->get ('/{id}/edit',   'App\Controllers\CategoriesController@edit');
    $this->post('/{id}/edit',   'App\Controllers\CategoriesController@save');
    $this->get ('/{id}/delete', 'App\Controllers\CategoriesController@delete');
});

/**
 * Executa as rotas da aplicaçao.
 */
$route->end();
