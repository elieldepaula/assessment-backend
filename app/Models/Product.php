<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Classe model de produtos.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class Product extends Model
{

    /**
     * Formata o campo 'price' para um formato monetario.
     * 
     * @param string $value
     * @return string
     */
    public function getPriceAttribute($value)
    {
        $value = $value / 100;
        return 'R$ ' . number_format($value, 2, ',', '.');
    }
    
    /**
     * Formata o campo 'price' para salvar no banco de dados.
     * 
     * @param string $value
     */
    public function setPriceAttribute($value = null)
    {
        if ($value != null) {
            $value = str_replace(['R$ ', '.'], '', $value);
            $value = str_replace(',', '.', $value);
            $value = $value * 100;
            $this->attributes['price'] = $value;
        }
    }
    
    /**
     * Relacionamento N:M de produtos e categorias.
     * @return mixed
     */
    public function category()
    {
        return $this->belongsToMany('App\Models\Category', 'product_category');
    }
    
}
