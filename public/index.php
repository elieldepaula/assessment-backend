<?php

/**
 * Front controller da aplicaçao, aqui definimos a constante de ambiente,
 * faço a conexao com o banco de dados, trato a exibiçao de mensagens de erro 
 * e alertas do PHP de acordo com o ambiente configurado e carrego o roteador.
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */

/**
 * Define o ambiente da aplicaçao.
 */
define('ENVIRONMENT', 'development');

date_default_timezone_set('Brazil/East');

/**
 * Define o caminho basico do projeto.
 */
define('DS', DIRECTORY_SEPARATOR, TRUE);
define('BASE_PATH', __DIR__ . DS . '..' . DS, TRUE);

/**
 * TRata a exibiçao de erros e mensagens do PHP de acordo com o ambiente.
 */
switch (ENVIRONMENT) {
    case 'development':
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        break;
    case 'production':
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(0);
        break;
}

/**
 * Carrega o loader do projeto.
 */
require BASE_PATH.'app/Loader.php';