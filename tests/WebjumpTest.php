<?php

define('ENVIRONMENT', 'development');
define('DS', DIRECTORY_SEPARATOR, TRUE);
define('BASE_PATH', __DIR__ . DS . '..' . DS, TRUE);

require BASE_PATH . 'vendor/autoload.php';
require BASE_PATH . 'app/Database.php';

use PHPUnit\Framework\TestCase;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;

/**
 * Testes unitarios do projeto, eles cobrem o basico de cadastro, alteraçao e
 * exclusao de categorias e produtos.
 * 
 * Nota: Execute com o banco de dados vazio para evitar perda de dados importantes
 * durante o teste de exclusao.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 */
class WebjumpTest extends TestCase
{
    
    public function testCreateCategory()
    {
        $category = new CategoryRepository();
        $object = (object) [
            'name' => 'PHPUnit_Test',
            'code' => '12345'
        ];
        $result = $category->new($object);
        $this->assertEquals(true, $result);
    }
    
    public function testEditCategory()
    {
        $category = new CategoryRepository();
        $object = (object) [
            'name' => 'PHPUnit_Test_alt',
            'code' => '54321'
        ];
        $result = $category->edit($object, 1);
        $this->assertEquals(true, $result);
    }
    
    public function testCreateProduct()
    {
        $product = new ProductRepository();
        $object = (object) [
            'sku' => 'SKU-1234',
            'name' => 'PHPUnit_Test',
            'description' => 'PHPUnit test.',
            'categories' => [1],
            'price' => 'R$ 215,99',
            'qty' => 50,
            'image' => null
        ];
        $result = $product->new($object);
        $this->assertEquals(true, $result);
    }
    
    public function testEditProduct()
    {
        $product = new ProductRepository();
        $object = (object) [
            'sku' => 'SKU-12345',
            'name' => 'PHPUnit_Test_alt',
            'description' => 'PHPUnit test modified.',
            'categories' => [1],
            'price' => 'R$ 115,99',
            'qty' => 40,
            'image' => null
        ];
        $result = $product->edit($object, 1);
        $this->assertEquals(true, $result);
    }
    
    public function testDeleteProduct()
    {
        $product = new ProductRepository();
        $result = $product->del(1);
        $this->assertEquals(true, $result);
    }
    
    public function testDeleteCategory()
    {
        $category = new CategoryRepository();
        $result = $category->del(1);
        $this->assertEquals(true, $result);
    }
    
}
